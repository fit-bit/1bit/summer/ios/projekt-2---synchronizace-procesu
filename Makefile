# Makefile
# Projekt 2 IOS 5.4.2019
# Autor: Matěj Kudera, FIT
# Přeloženo: gcc 7.4
# Makefile pro složení programu

CC = gcc
FLAGS = -std=gnu99 -Wall -Wextra -Werror -pedantic
THREADS = -pthread

all: proj2

proj2: proj2.o
	$(CC) $(FLAGS) proj2.o -o proj2 $(THREADS)

proj2.o: proj2.c
	$(CC) $(FLAGS) -c proj2.c -o proj2.o $(THREADS)

## Příkazy ##
clean:
	-rm -f proj2 *.o xkuder04.zip

zip: 
	zip xkuder04.zip *.c *.h Makefile