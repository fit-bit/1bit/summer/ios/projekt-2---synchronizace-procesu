// proj2.c
// Projekt 2 IOS 5.4.2019
// Autor: Matěj Kudera, FIT
// Přeloženo: gcc 7.4
// Řešení problému synchronizace procesů

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <semaphore.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>

// Na testy
//# define DEBUG

#ifdef DEBUG
# define DEBUG_PRINT(x) printf x
#else
# define DEBUG_PRINT(x) do {} while (0)
#endif

/////////////////// Erorr zprávy //////////////////
char *argument_error_message = "Spuštení:\n\
./proj2 P H S R W C\n\
\n\
• P je počet osob generovaných v každé kategorii; bude vytvořeno P hackers a P serfs.\n\
P >= 2 && (P % 2) == 0\n\
• H je maximální hodnota doby (v milisekundách), po které je generován nový proces hackers.\n\
H >= 0 && H <= 2000.\n\
• S je maximální hodnota doby (v milisekundách), po které je generován nový proces serfs.\n\
S >= 0 && S <= 2000.\n\
• R je maximální hodnota doby (v milisekundách) plavby.\n\
R >= 0 && R <= 2000.\n\
• W je maximální hodnota doby (v milisekundách), po které se osoba vrací zpět na molo (pokud bylo před tím molo plné).\n\
W >= 20 && W <= 2000.\n\
• C je kapacita mola. C >= 5.\n\
• Všechny parametry jsou celá čísla.";

/////////////////// Prototypy funkcí //////////////////
void print_error_and_exit(char *message);
int check_digit(char *argument);
int check_arguments(int argc, char *argv[]);
void generate_hackers();
void generate_serfs();
void hacker(unsigned id);
void serf(unsigned id);
int init();
int destroy();
unsigned rand_number_in_interval(unsigned upper, unsigned lower);

/////////////////// Parametry z příkazové řádky ///////////////
// P je počet osob generovaných v každé kategorii; bude vytvořeno P hackers a P serfs.
unsigned P;
// H je maximální hodnota doby (v milisekundách), po které je generován nový proces hackers.
unsigned H;
// S je maximální hodnota doby (v milisekundách), po které je generován nový proces serfs.
unsigned S;
// R je maximální hodnota doby (v milisekundách) plavby.
unsigned R;
// W je maximální hodnota doby (v milisekundách), po které se osoba vrací zpět na molo (pokud bylo před tím molo plné).
unsigned W;
//  C je kapacita mola.
unsigned C;

///////////////////// Semafory /////////////////////////
sem_t *write_output_sem; // Semafor pro zápis do souboru
sem_t *print_err_sem; // Semafor kdyby náhodou nastaly dvě chyby zarás
sem_t *enter_mole_sem; // Semafor vstupu na molo
sem_t *waiting_hackers_sem; // Hackers připravení na plavbu
sem_t *waiting_serfs_sem; // Serfs připravení na plavbu
sem_t *make_grupes_sem; // Semafor pro vybýrání skupinek k plavbě
sem_t *save_change_mole_parameters_sem; // Semafor pro bezpečnou úpravu parametrů mola
sem_t *leaving_ship_sem; // Kontroluje aby se procesi co odchází z lodi zapisolavi postupně
sem_t *end_of_sail_sem; // Zajistí aby kapitán vystoupil jako poslední


//////////////////// Zdílené proměnné ///////////////////
unsigned *NS = NULL; // Aktuální počet serfs na molu
unsigned *NH = NULL; // Aktuální počet hackers na molu
unsigned *A = NULL; // Řádek výpisu
unsigned *leaved_ship = NULL; // Počítadlo kolik procesů opustilo loď
FILE *output = NULL; // Výstupní soubor

//////////////// Pomocné Funkce ////////////////////
// Generování random hodnoty v intervalu
unsigned rand_number_in_interval(unsigned upper, unsigned lower)
{
    if (upper == 0)
    {
        return 0;
    }

    return (rand() % (upper - lower + 1) + lower);
}

// Výpis pokud nastala chyba
void print_error_and_exit(char *message)
{
    fprintf(stderr, "%s\n", message);
    exit(1);
}

// Kontola jestli string je celé číslo
int check_digit(char *argument)
{
    for (int i = 0; (unsigned)i < strlen(argument); i++)
    {
        if (!isdigit(argument[i]))
        {
            return 1;
        }
    }
    return 0;
}

// Kontrola argumentů
int check_arguments(int argc, char *argv[])
{
    // správný počet argumentů
    if (argc != 7)
    {
        return 1;
    }

    // Kontrole jestli všechny argumenty jsou celá čísla
    for (int i = 1; i < argc; i++)
    {
        if (check_digit(argv[i]) != 0)
        {
            return 1;
        }
    }

    // Načtení argumentů do globálních proměnných
    P = strtol(argv[1], NULL, 10);
    H = strtol(argv[2], NULL, 10);
    S = strtol(argv[3], NULL, 10);
    R = strtol(argv[4], NULL, 10);
    W = strtol(argv[5], NULL, 10);
    C = strtol(argv[6], NULL, 10);

    // Vzhledem ke dřívější kontrole jestli je to číslo už nemusím kontrolovat jestli je číslo < 0

    // Kontrola jestli P odpovídá P >= 2 && (P % 2) == 0
    if (P < 2 || (P % 2) != 0)
    {
        return 1;
    }

    // Kontrola jestli H odpovídá H >= 0 && H <= 2000
    if (H > 2000)
    {
        return 1;
    }

    // Kontrola jestli S odpovídá S >= 0 && S <= 2000
    if (S > 2000)
    {
        return 1;
    }

    // Kontrola jestli R odpovídá R >= 0 && R <= 2000
    if (R > 2000)
    {
        return 1;
    }

    // Kontrola jestli W odpovídá W >= 20 && W <= 2000
    if (W < 20 || W > 2000)
    {
        return 1;
    }

    // Kontrola jestli C odpovídá C >= 5
    if (C < 5)
    {
        return 1;
    }

    return 0;
}

// Generátor Hackers
void generate_hackers()
{
    pid_t hacker_id = 0;
    unsigned I; // Pořadové číslo procesu hacker

    // Generování procesů hackers
    for(I = 1; I <= P; I++)
    {
        // Vytvoření potomka
        hacker_id = fork();
        if (hacker_id == 0)
        {
            // Proces hacker
            hacker(I);
        }
        else if (hacker_id < 0)
        {
            // Nepodařil se fork, vše ukončíme
            sem_wait(print_err_sem);
                fprintf(stderr, "%s\n", "Nepodařilo se vytvořit nějaký proces Hacker");
            sem_post(print_err_sem);

            kill(0,SIGKILL); // Ukončí všechny procesy ve skupině
            destroy();
            exit(1);
        }

        // Čas za který se bude generovat další hacker
        usleep(rand_number_in_interval(H,0) * 1000);
    }

    // Čekání na dokončení potomků
    for (unsigned i = 0; i < P; i++)
    {
        wait(NULL);
    }

    exit(0);
}

// Generátor Serfs
void generate_serfs()
{
    pid_t serf_id = 0;
    unsigned I; // Pořadové číslo procesu serf

    // Generování procesů hackers
    for(I = 1; I <= P; I++)
    {
        // Vytvoření potomka
        serf_id = fork();
        if (serf_id == 0)
        {
            // Proces Serf
            serf(I);
        }
        else if (serf_id < 0)
        {
            // Nepodařil se fork, vše ukončíme
            sem_wait(print_err_sem);
                fprintf(stderr, "%s\n", "Nepodařilo se vytvořit nějaký proces Serf");
            sem_post(print_err_sem);

            kill(0,SIGKILL); // Ukončí všechny procesy ve skupině
            destroy();
            exit(1);
        }

        // Čas za který se bude generovat další serf
        usleep(rand_number_in_interval(S,0) * 1000); 
    }

    // Čekání na dokončení potomků
    for (unsigned i = 0; i < P; i++)
    {
        wait(NULL);
    }

    exit(0);
}

// Proces hacker
void hacker(unsigned id)
{
    // Oznámení že žije
    sem_wait(write_output_sem);
        (*A)++; /// Zvíšení počítadla výpisu
        fprintf(output, "%u: HACK %u: starts\n", *A, id);
        fflush(output);
        DEBUG_PRINT(("%u: HACK %u: starts\n", *A, id));
    sem_post(write_output_sem);

    // Pokus dostat se na molo
    while (1)
    {
        sem_wait(enter_mole_sem);
        if ((*NS)+(*NH) == C)
        {
            // Molo je plné. Vypíše zprávu že čeká
            sem_wait(write_output_sem);
                (*A)++; /// Zvíšení počítadla výpisu
                fprintf(output, "%u: HACK %u: leaves queue: %u: %u\n", *A, id, *NH, *NS);
                fflush(output);
                DEBUG_PRINT(("%u: HACK %u: leaves queue: %u: %u\n", *A, id, *NH, *NS));
            sem_post(write_output_sem);

            // Další múžou jít na molo
            sem_post(enter_mole_sem);

            // Čeká
            usleep(rand_number_in_interval(W,20) * 1000);

            // Vypíše I am back
            sem_wait(write_output_sem);
                (*A)++; /// Zvíšení počítadla výpisu
                fprintf(output, "%u: HACK %u: is back\n", *A, id);
                fflush(output);
                DEBUG_PRINT(("%u: HACK %u: is back\n", *A, id));
            sem_post(write_output_sem);
        }
        else
        {
            // Dostal se na molo
            sem_wait(save_change_mole_parameters_sem);
            (*NH)++;

            // Vypíše že je na molu
            sem_wait(write_output_sem);
                (*A)++; /// Zvíšení počítadla výpisu
                fprintf(output, "%u: HACK %u: waits: %u: %u\n", *A, id, *NH, *NS);
                fflush(output);
                DEBUG_PRINT(("%u: HACK %u: waits: %u: %u\n", *A, id, *NH, *NS));
            sem_post(write_output_sem);

            sem_post(save_change_mole_parameters_sem);

            // Další múžou jít na molo
            sem_post(enter_mole_sem);

            // Podívá se jestli může udělat skupinu
            if ((*NH) == 4)
            {
                sem_wait(make_grupes_sem);

                // Snížíme počet hackerů na molu
                sem_wait(save_change_mole_parameters_sem);
                (*NH) -= 4;

                // Nastavení proměnné která zajistí aby kapitán vystoupil poslední
                (*leaved_ship) = 0;

                // Proces co našel skupinu se stane kapinánem 
                sem_wait(write_output_sem);
                    (*A)++; /// Zvíšení počítadla výpisu
                    fprintf(output, "%u: HACK %u: boards: %u: %u\n", *A, id, *NH, *NS);
                    fflush(output);
                    DEBUG_PRINT(("%u: HACK %u: boards: %u: %u\n", *A, id, *NH, *NS));
                sem_post(write_output_sem);

                sem_post(save_change_mole_parameters_sem);

                // Uspání kapinána pro simulování plavby
                usleep(rand_number_in_interval(R,0) * 1000);

                // Nechá vystoupit ostatní a pak vypíše že odchází taky
                sem_post(waiting_hackers_sem);
                sem_post(waiting_hackers_sem);
                sem_post(waiting_hackers_sem);

                // Počká než všichni vystoupí
                sem_wait(end_of_sail_sem);

                sem_wait(write_output_sem);
                    (*A)++; /// Zvíšení počítadla výpisu
                    fprintf(output, "%u: HACK %u: captain exits: %u: %u\n", *A, id, *NH, *NS);
                    fflush(output);
                    DEBUG_PRINT(("%u: HACK %u: captain exits: %u: %u\n", *A, id, *NH, *NS));
                sem_post(write_output_sem);

                sem_post(make_grupes_sem);
            }
            else if ((*NH) == 2 && (*NS) >= 2)
            {
                sem_wait(make_grupes_sem);

                // Snížíme počet hackerů a serfs na molu
                sem_wait(save_change_mole_parameters_sem);
                (*NH) -= 2;
                (*NS) -= 2;

                // Nastavení proměnné která zajistí aby kapitán vystoupil poslední
                (*leaved_ship) = 0;

                // Proces co našel skupinu se stane kapinánem 
                sem_wait(write_output_sem);
                    (*A)++; /// Zvíšení počítadla výpisu
                    fprintf(output, "%u: HACK %u: boards: %u: %u\n", *A, id, *NH, *NS);
                    fflush(output);
                    DEBUG_PRINT(("%u: HACK %u: boards: %u: %u\n", *A, id, *NH, *NS));
                sem_post(write_output_sem);

                sem_post(save_change_mole_parameters_sem);

                // Uspání kapinána pro simulování plavby
                usleep(rand_number_in_interval(R,0) * 1000);

                // Nechá vystoupit ostatní a pak vypíše že odchází taky
                sem_post(waiting_hackers_sem);
                sem_post(waiting_serfs_sem);
                sem_post(waiting_serfs_sem);

                // Počká než všichni vystoupí
                sem_wait(end_of_sail_sem);

                sem_wait(write_output_sem);
                    (*A)++; /// Zvíšení počítadla výpisu
                    fprintf(output, "%u: HACK %u: captain exits: %u: %u\n", *A, id, *NH, *NS);
                    fflush(output);
                    DEBUG_PRINT(("%u: HACK %u: captain exits: %u: %u\n", *A, id, *NH, *NS));
                sem_post(write_output_sem);

                sem_post(make_grupes_sem);
            }
            else
            {
                // Na molu není vhodná skupinka pro začátek plavby
                // Čeká než bude zavolán že je potřeba
                sem_wait(waiting_hackers_sem);

                // Na plavbu nastupuje jako cestující, kapitán bude proces co najde vhodnou skupinu
                // Když je probuzen kapitánem, vypíše že byl na plavbě a končí
                sem_wait(write_output_sem);
                    (*A)++; /// Zvíšení počítadla výpisu
                    fprintf(output, "%u: HACK %u: member exits: %u: %u\n", *A, id, *NH, *NS);
                    fflush(output);
                    DEBUG_PRINT(("%u: HACK %u: member exits: %u: %u\n", *A, id, *NH, *NS));
                sem_post(write_output_sem);

                // Sdělí kapitánovy že odešel
                sem_wait(leaving_ship_sem);
                    (*leaved_ship)++;
                    if ((*leaved_ship) == 3) sem_post(end_of_sail_sem);
                sem_post(leaving_ship_sem);
            }

            exit(0);
        }       
    }
}

// proces serf
void serf(unsigned id)
{
    // Oznámení že žije
    sem_wait(write_output_sem);
        (*A)++; /// Zvíšení počítadla výpisu
        fprintf(output, "%u: SERF %u: starts\n", *A, id);
        fflush(output);
        DEBUG_PRINT(("%u: SERF %u: starts\n", *A, id));
    sem_post(write_output_sem);

    // Pokus dostat se na molo
    while (1)
    {
        sem_wait(enter_mole_sem);
        if ((*NS)+(*NH) == C)
        {
            // Molo je plné. Vypíše zprávu že čeká
            sem_wait(write_output_sem);
                (*A)++; /// Zvíšení počítadla výpisu
                fprintf(output, "%u: SERF %u: leaves queue: %u: %u\n", *A, id, *NH, *NS);
                fflush(output);
                DEBUG_PRINT(("%u: SERF %u: leaves queue: %u: %u\n", *A, id, *NH, *NS));
            sem_post(write_output_sem);

            // Další múžou jít na molo
            sem_post(enter_mole_sem);

            // Čeká
            usleep(rand_number_in_interval(W,20) * 1000);

            // Vypíše I am back
            sem_wait(write_output_sem);
                (*A)++; /// Zvíšení počítadla výpisu
                fprintf(output, "%u: SERF %u: is back\n", *A, id);
                fflush(output);
                DEBUG_PRINT(("%u: SERF %u: is back\n", *A, id));
            sem_post(write_output_sem);
        }
        else
        {
            // Dostal se na molo
            sem_wait(save_change_mole_parameters_sem);
            (*NS)++;

            // Vypíše že je na molu
            sem_wait(write_output_sem);
                (*A)++; /// Zvíšení počítadla výpisu
                fprintf(output, "%u: SERF %u: waits: %u: %u\n", *A, id, *NH, *NS);
                fflush(output);
                DEBUG_PRINT(("%u: SERF %u: waits: %u: %u\n", *A, id, *NH, *NS));
            sem_post(write_output_sem);

            sem_post(save_change_mole_parameters_sem);

            // Další múžou jít na molo
            sem_post(enter_mole_sem);

            // Podívá se jestli může udělat skupinu
            if ((*NS) == 4)
            {
                sem_wait(make_grupes_sem);

                // Snížíme počet serfs na molu
                sem_wait(save_change_mole_parameters_sem);
                (*NS) -= 4;

                // Nastavení proměnné která zajistí aby kapitán vystoupil poslední
                (*leaved_ship) = 0;

                // Proces co našel skupinu se stane kapinánem 
                sem_wait(write_output_sem);
                    (*A)++; /// Zvíšení počítadla výpisu
                    fprintf(output, "%u: SERF %u: boards: %u: %u\n", *A, id, *NH, *NS);
                    fflush(output);
                    DEBUG_PRINT(("%u: SERF %u: boards: %u: %u\n", *A, id, *NH, *NS));
                sem_post(write_output_sem);

                sem_post(save_change_mole_parameters_sem);

                // Uspání kapinána pro simulování plavby
                usleep(rand_number_in_interval(R,0) * 1000);

                // Nechá vystoupit ostatní a pak vypíše že odchází taky
                sem_post(waiting_serfs_sem);
                sem_post(waiting_serfs_sem);
                sem_post(waiting_serfs_sem);

                // Počká než všichni vystoupí
                sem_wait(end_of_sail_sem);

                sem_wait(write_output_sem);
                    (*A)++; /// Zvíšení počítadla výpisu
                    fprintf(output, "%u: SERF %u: captain exits: %u: %u\n", *A, id, *NH, *NS);
                    fflush(output);
                    DEBUG_PRINT(("%u: SERF %u: captain exits: %u: %u\n", *A, id, *NH, *NS));
                sem_post(write_output_sem);

                sem_post(make_grupes_sem);
            }
            else if ((*NS) == 2 && (*NH) >= 2)
            { 
                sem_wait(make_grupes_sem);

                // Snížíme počet hackerů a serfs na molu
                sem_wait(save_change_mole_parameters_sem);
                (*NH) -= 2;
                (*NS) -= 2;

                // Nastavení proměnné která zajistí aby kapitán vystoupil poslední
                (*leaved_ship) = 0;

                // Proces co našel skupinu se stane kapinánem 
                sem_wait(write_output_sem);
                    (*A)++; /// Zvíšení počítadla výpisu
                    fprintf(output, "%u: SERF %u: boards: %u: %u\n", *A, id, *NH, *NS);
                    fflush(output);
                    DEBUG_PRINT(("%u: SERF %u: boards: %u: %u\n", *A, id, *NH, *NS));
                sem_post(write_output_sem);

                sem_post(save_change_mole_parameters_sem);

                // Uspání kapinána pro simulování plavby
                usleep(rand_number_in_interval(R,0) * 1000);

                // Nechá vystoupit ostatní a pak vypíše že odchází taky
                sem_post(waiting_hackers_sem);
                sem_post(waiting_hackers_sem);
                sem_post(waiting_serfs_sem);

                // Počká než všichni vystoupí
                sem_wait(end_of_sail_sem);

                sem_wait(write_output_sem);
                    (*A)++; /// Zvíšení počítadla výpisu
                    fprintf(output, "%u: SERF %u: captain exits: %u: %u\n", *A, id, *NH, *NS);
                    fflush(output);
                    DEBUG_PRINT(("%u: SERF %u: captain exits: %u: %u\n", *A, id, *NH, *NS));
                sem_post(write_output_sem);

                sem_post(make_grupes_sem);
            }
            else
            {
                // Na molu není vhodná skupinka pro začátek plavby
                // Čeká než bude zavolán že je potřeba
                sem_wait(waiting_serfs_sem);

                // Na plavbu nastupuje jako cestující, kapitán bude proces co najde vhodnou skupinu
                // Když je probuzen kapitánem, vypíše že byl na plavbě a končí
                sem_wait(write_output_sem);
                    (*A)++; /// Zvíšení počítadla výpisu
                    fprintf(output, "%u: SERF %u: member exits: %u: %u\n", *A, id, *NH, *NS);
                    fflush(output);
                    DEBUG_PRINT(("%u: SERF %u: member exits: %u: %u\n", *A, id, *NH, *NS));
                sem_post(write_output_sem);

                // Sdělí kapitánovy že odešel
                sem_wait(leaving_ship_sem);
                    (*leaved_ship)++;
                    if ((*leaved_ship) == 3) sem_post(end_of_sail_sem);
                sem_post(leaving_ship_sem);
            }

            exit(0);
        }       
    }
}

// Inicializace semaforů a sdílených proměnných
int init()
{
    // Semafory
    write_output_sem = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, 0, 0);
    if (write_output_sem == MAP_FAILED) return 1;
    if (sem_init(write_output_sem, 1, 1) == -1) return 1;

    print_err_sem = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, 0, 0);
    if (print_err_sem == MAP_FAILED) return 1;
    if (sem_init(print_err_sem, 1, 1) == -1) return 1;

    enter_mole_sem = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, 0, 0);
    if (enter_mole_sem == MAP_FAILED) return 1;
    if (sem_init(enter_mole_sem, 1, 1) == -1) return 1;

    waiting_hackers_sem = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, 0, 0);
    if (waiting_hackers_sem == MAP_FAILED) return 1;
    if (sem_init(waiting_hackers_sem, 1, 0) == -1) return 1;

    waiting_serfs_sem = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, 0, 0);
    if (waiting_serfs_sem == MAP_FAILED) return 1;
    if (sem_init(waiting_serfs_sem, 1, 0) == -1) return 1;

    make_grupes_sem = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, 0, 0);
    if (make_grupes_sem == MAP_FAILED) return 1;
    if (sem_init(make_grupes_sem, 1, 1) == -1) return 1;

    save_change_mole_parameters_sem = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, 0, 0);
    if (save_change_mole_parameters_sem == MAP_FAILED) return 1;
    if (sem_init(save_change_mole_parameters_sem, 1, 1) == -1) return 1;

    end_of_sail_sem = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, 0, 0);
    if (end_of_sail_sem == MAP_FAILED) return 1;
    if (sem_init(end_of_sail_sem, 1, 0) == -1) return 1;

    leaving_ship_sem = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, 0, 0);
    if (leaving_ship_sem == MAP_FAILED) return 1;
    if (sem_init(leaving_ship_sem, 1, 1) == -1) return 1;

    // Sdílené proměnné
    A = mmap(NULL, sizeof(unsigned), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, 0, 0);
    if (A == MAP_FAILED) return 1;

    NH = mmap(NULL, sizeof(unsigned), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, 0, 0);
    if (NH == MAP_FAILED) return 1;

    NS = mmap(NULL, sizeof(unsigned), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, 0, 0);
    if (NS == MAP_FAILED) return 1;

    leaved_ship = mmap(NULL, sizeof(unsigned), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, 0, 0);
    if (leaved_ship == MAP_FAILED) return 1;

    return 0;
}


// Smazání semaforů a sdílených proměnných
int destroy()
{
    // Uzavření souboru pro výpis
    if (output != NULL)
    {
        fclose(output);
    }

    // Semafory
    sem_destroy(write_output_sem);
    munmap(write_output_sem, sizeof(sem_t));

    sem_destroy(print_err_sem);
    munmap(print_err_sem, sizeof(sem_t));

    sem_destroy(enter_mole_sem);
    munmap(enter_mole_sem, sizeof(sem_t));

    sem_destroy(waiting_hackers_sem);
    munmap(waiting_hackers_sem, sizeof(sem_t));

    sem_destroy(waiting_serfs_sem);
    munmap(waiting_serfs_sem, sizeof(sem_t));

    sem_destroy(make_grupes_sem);
    munmap(make_grupes_sem, sizeof(sem_t));

    sem_destroy(save_change_mole_parameters_sem);
    munmap(save_change_mole_parameters_sem, sizeof(sem_t));

    sem_destroy(end_of_sail_sem);
    munmap(end_of_sail_sem, sizeof(sem_t));

    sem_destroy(leaving_ship_sem);
    munmap(leaving_ship_sem, sizeof(sem_t));

    // Sdílené proměnné
    munmap(A, sizeof(unsigned));
    munmap(NH, sizeof(unsigned));
    munmap(NS, sizeof(unsigned));
    munmap(leaved_ship, sizeof(unsigned));

    return 0;
}

///////////////////////// Main /////////////////////////////
int main (int argc, char *argv[])
{
    // Kontrola argumentů
    if (check_arguments(argc, argv) != 0)
    {
        print_error_and_exit(argument_error_message);
    }

    // Inicializace semaforů a sdílených proměnných
    if (init() != 0)
    {
        destroy();
        print_error_and_exit("Chyba při inicializování semaforů a sdílených proměnných");
    }

    // Otevření souboru pro výpis
    output = fopen("proj2.out", "w");
    if (output == NULL)
    {
        destroy();
        print_error_and_exit("Nepodařilo se otevřít soubor pro zápis informací proj2.out");
    }

    // Vytvoření dvou pomocných procesů pro generování procesů osob stejné kategorie.
    pid_t pid_hackers = 0;
    pid_t pid_serfs = 0;

    srand(time(NULL)); // Inicializace pro random generátor
    pid_hackers = fork();
    if (pid_hackers == 0)
    {
        // Proces Generátor Hackers
        generate_hackers();
    }
    else if (pid_hackers > 0)
    {
        // Pokračování procesu main
        pid_serfs = fork();
        if (pid_serfs == 0)
        {
            // Proces Generátor Serfs
            generate_serfs();
        }
        else if (pid_serfs < 0)
        {
            // Nepodařil se fork, vše ukončíme
            sem_wait(print_err_sem);
                fprintf(stderr, "%s\n", "Nepodařilo se vytvořit proces generátor procesů Serfs");
            sem_post(print_err_sem);

            kill(0,SIGKILL); // Ukončí všechny procesy ve skupině
            destroy();
            exit(1);
        }
    }
    else
    {
        // Nepodařilo se vytvořit potomka
        destroy();
        print_error_and_exit("Nepodařilo se vytvořit proces generátor procesů Hackers");
    }
    
    // Čekání na ukončení procesů
    waitpid(pid_hackers, NULL, 0);
    waitpid(pid_serfs, NULL, 0);

    // Když vše dopadlo dobře ukončení s kódem (exit code) 0
    destroy();
    return 0;
}
